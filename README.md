## Info

This is a Dockerfile to build a container image for nginx and php-fpm designed for including support for runtime re-configurations.

## The Public (or Web Visible) Volume

This volume is a read only volume and the actual folder served by Nginx. It should only contain files which are suppose to be accessible via HTTP protocol, so no putting configuration files or other sensitive data in here.

The volume location inside your container will be `/data/www`.

A good standard is to for your project to have or generate a `public` folder for you to map to this volume:

```
docker run \
	--name my-ln_p \
	-v $(pwd)/public:/data/www \
	-p 8080:80 \
	vredens/ln_p
```

## The ReadOnly Volume

The read-only volume is aimed at providing a folder where you can place your source code. Note that Nginx will not serve any files from this location. You should instead include these files in your public scripts.

The volume location inside your container will be `/data/lib`.

Unless your source code is creating files in the wrong place, it is safe to bind the root of your project to this volume

```
docker run \
	--name my-ln_p \
	-v $(pwd):/data/lib \
	-v $(pwd)/public:/data/www \
	-p 8080:80 \
	vredens/ln_p
```

## The ReadOnly Volume for configurations and init scripts

This volume is aimed at providing a folder where you can link your configuration files and init scripts for your container. Your `docker-boot.sh` file must be placed at the root of the folder.

Your best use for this volume is having your environment configurations on a specific folder and linking to this volume so it is available inside the container.

```
docker run \
	--name my-ln_p \
	-v $(pwd):/data/lib \
	-v $(pwd)/init/dev-environment:/data/etc \
	-v $(pwd)/public:/data/www \
	-p 8080:80 \
	vredens/ln_p
```

## The ReadWrite Volume

This volume should be used to create files. If you need PHP or Nginx to write to a sub-folder you create make sure to use the provided bash function `create_rw_sub_folder $foldername` which will be available to your `docker-boot.sh` script.

## The `docker-boot.sh`

This container allows you to provide a file named `docker-boot.sh` on the root of your read-only volume destined for configuration files and init scripts:

  * `/data/etc/`

This script will be sourced (Bash's version of PHP's `include`) by the container's boot script. There are a few scripts and functions which will be available to you

  * Bash function `create_rw_sub_folder $foldername`, this function allows you to safely create a sub folder under the ReadWrite Volume which will be writable by the user running the Nginx and PHP-FPM processes.
  * Script `/vs-configurator`, see the Configurator section below

## Configurator

There is a special script, part of the [Vredenslack Scripts](https://bitbucket.org/eeriesoftronics/vredenslack) collection, which is provided inside the container for settings up configuration files when booting a container instance. This is a Perl script which will parse files and replace placeholders with the proper value, if there is one. You can, and should, check its documentation [here](https://bitbucket.org/eeriesoftronics/vredenslack/raw/master/docs/vs-configurator.md).

On your `docker-boot.sh` you can add a line such as this

```
/vs-configurator -l /data/etc/config.ini translate /data/lib/app/config/config.php.dist /data/run/config/config.php
```

This will create the `/data/run/config/config.php` file using `/data/lib/app/config/config.php.dist` as template and the (optional) `/data/etc/config.ini` file with settings for all placeholders (`{%...%}`). The configurator will output all replacements and errors. You can redirect the output to a file if running your container in detached mode.
