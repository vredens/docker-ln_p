FROM webdevops/php-nginx:debian-8
MAINTAINER JB Ribeiro <vredens@gmail.com>

# Override the default runtime user (1000:1000), this is so we can execute our
# own /start.sh as root
ENV APPLICATION_USER root
ENV APPLICATION_GROUP root
ENV APPLICATION_UID 0
ENV APPLICATION_GID 0
# Used to configure NGINX
ENV WEB_DOCUMENT_ROOT /data/www
ENV WEB_DOCUMENT_INDEX index.php
# Used for run time configurations
ENV RUN_VOLUME_PATH /data/run
ENV LIB_VOLUME_PATH /data/lib
ENV ETC_VOLUME_PATH /data/etc

# Setup ww-data writable folders for logging, fs caching, etc
RUN mkdir -p /data/run && mkdir -p /data/etc && mkdir -p /data/www && mkdir -p /data/lib && chown -Rf 1000:1000 /data/run/ /data/www/ /data/lib/ /data/etc

# Public Volume (read-only files accessible via HTTP protocol)
VOLUME ["/data/www"]

# Read-Only Volume (for configuration files or boot scripts)
VOLUME ["/data/etc"]

# Read-Only Volume (for read-only files)
VOLUME ["/data/lib"]

# Read-Write Volume (for read-write files, specially runtime such as logging, temp files, etc)
VOLUME ["/data/run"]

# Start Supervisord
ADD scripts/ln_p-boot.sh /ln_p-boot.sh
ADD scripts/vs-configurator /vs-configurator
RUN chmod 755 /ln_p-boot.sh /vs-configurator

# The command will be executed by the entrypoint script
CMD ["/bin/bash", "/ln_p-boot.sh"]
