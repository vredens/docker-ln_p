#!/usr/bin/env perl

package Configurator;

use 5.010;
use strict;
use warnings;

use Carp;
use Data::Dumper;

our $replace = 1;

# match and grab {%config1:config2:-defaultval%} where anything after a ':' is optional
# grab words plus - and . between {} and make sure the starting { isn't escaped \{, and start a continuous greedy matching
# the default value is a non-greedy collection of a positive number of characters
# REVIEW: default value currently does not support using "\%}" for including %} in the final expression, this is a minor issue
our $templating_regex = qr/(?:[^\\]|^) {% ( \w+ (?:[-\.:]\w+)* ( :- .*? )? ) %}/x;

#===============================================================================
sub new {
#===============================================================================
	bless {}, shift;
}

#===============================================================================
sub valid_key {
#===============================================================================
	defined $_[0] and $_[0] =~ /^\w+([-\.]\w+)*$/;
}

#===============================================================================
sub add_translation {
#===============================================================================
	my $self = shift;
	my ($key, $val) = @_;

	$key = 'config.' . $key;

	croak 'invalid translation key' unless valid_key($key);

	$self->{$key} = $val if not defined $self->{$key} or $replace;
}

#===============================================================================
sub add_translations {
#===============================================================================
	my $self = shift;
	my %translations = @_;
	my $v;

	foreach my $k (keys %translations) {
		my $_k = 'config.' . $k;

		croak 'invalid translation key' unless valid_key($_k);

		$self->{$_k} = $translations{$k} if not defined $self->{$_k} or $replace;
	}
}

#===============================================================================
sub set_translation {
#===============================================================================
	my $self = shift;
	my ($key, $val) = @_;

	$key = 'config.' . $key;

	croak 'invalid translation key' unless valid_key($key);

	$self->{$key} = $val;
}

#===============================================================================
sub get_translation {
#===============================================================================
	my $self = shift;

	foreach (@_) {
		if (/^-(.*)$/) {
			return ('default', $1) if wantarray;
			return $1
		}
		if (defined $self->{$_}) {
			return ($_, $self->{$_}) if wantarray;
			return $self->{$_};
		}
	}

	return (undef, undef) if wantarray;
	return undef;
}

#===============================================================================
sub translate_placeholder {
#===============================================================================
	my $self = shift;
	my $ph = shift;

	my @keys = $self->_parse_placeholder($ph);

	$self->get_translation(@keys);
}

#===============================================================================
sub list_translations {
#===============================================================================
	my $self = shift;

	# TODO: replace by freeze-thaw / clone?
	my $tr = {};
	foreach (keys %{$self}) {
		$tr->{$_} = $self->{$_};
	}

	return $tr;
}

#===============================================================================
sub translate {
#===============================================================================
	my $self = shift;
	my $str = shift;
	my $ignore_errors = shift;

	my $report = {
		'success' => {},
		'failed'  => {}
	};

	while ($str =~ /$templating_regex/gc) {
		my $ph = $1;
		my $v = $self->translate_placeholder($ph);

		if (defined $v) {
			$str =~ s/{%\Q$ph\E%}/$v/g;
			$report->{success}->{$ph} = $v;
		} else {
			# increment the failed translation counter
			$report->{failed}->{$ph} = 0 unless defined $report->{failed}->{$ph};
			$report->{failed}->{$ph}++;
		}
	}

	return ($report, $str) if wantarray;

	$str;
}

#===============================================================================
sub parse {
#===============================================================================
	my $self = shift;
	my ($src, $dst) = @_;
	my $failed = 0;

	my $report = {
		'success' => {},
		'failed'  => {}
	};

	open (my $fh, '<', $src) or die 'failed to open [', $src, ']';
	open (my $ofh, '>', $dst) or die 'failed to open [', $dst, '] for writting' if defined $dst;
	while(<$fh>) {
		while (/$templating_regex/gc) {
			my $ph = $1;
			my $v = $self->translate_placeholder($ph);

			if (defined $v) {
				s/{%\Q$ph\E%}/$v/g;
				$report->{success}->{$ph} = $v;
			} else {
				# increment the failed translation counter
				$report->{failed}->{$ph} = 0 unless defined $report->{failed}->{$ph};
				$report->{failed}->{$ph}++;
			}
		}

		print $ofh $_ if defined $dst;
	}
	close($fh);
	close($ofh) if defined $dst;

	$report;
}

#===============================================================================
sub load_translations {
#===============================================================================
	my $self = shift;
	my $file = shift;

	if ($file eq 'env') {
		$self->_env_loader();
	} else {
		$self->_txt_loader($file);
	}
}

#===============================================================================
sub _parse_placeholder {
#===============================================================================
	my $self = shift;
	my $ph = shift;

	return split /:/, $ph;
}

#===============================================================================
sub _txt_loader {
#===============================================================================
	my $self = shift;
	my $file = shift;

	my $line = 0;
	open( my $fh, '<', $file ) or croak 'failed to load TEXT translation file ', $file;
	while (<$fh>) {
		$line++;
		# ignore lines that start with ; # or are empty lines
		unless (/^\s*(?:(;|#).*)?$/) {
			if (/^([^:=]+)(?::|=)(.*)$/) {
				my ($k, $v) = ($1, $2);

				$self->add_translation($k, $v);
				#croak 'invalid translation entry in ', $file, ' at line ', $line unless valid_key($k);
				#$self->{'config.' . $k} = $v;
			} else {
				die 'invalid entry in ', $file, ' at line ', $line;
			}
		}
	}
	close ($fh);
}

#===============================================================================
sub _env_loader {
#===============================================================================
	my $self = shift;

	foreach (keys %ENV) {
		my $k = 'env.' . $_;
		$self->{$k} = $ENV{$_} if valid_key($k);
	}
}

#===============================================================================
sub _flatten {
#===============================================================================
	my $self = shift;
	my $href = shift;
	my $t = shift || '';

	if (ref $href eq 'HASH') {
		foreach my $k (keys %{$href}) {
			my $_t = $t ? $t.'.'.$k : $k;
			if (! ref ($href->{$k})) {
				$self->add_translation($_t, defined $href->{$k} ? $href->{$k} : '');
			} else {
				$self->_flatten($href->{$k}, $_t);
			}
		}
	} else {
		croak 'bad translation structure: ', ref $href;
	}
}

1;

package main;

use 5.010;
use strict;
use warnings;

use Data::Dumper;
use File::Basename;
use File::Path 'make_path';
use Cwd 'abs_path';
#use Configurator;
use Getopt::Long;
use Pod::Usage;
use Term::ANSIColor ':constants';

my $opts = {
	'env' => 1,
	'quiet' => 0,
	'ignore-failed' => 0,
};

GetOptions($opts,
	'translation|t=s%',
	'load|l=s@',
	'env!',
	'quiet|q',
	'ignore-failed',

	'man',
	'help',
);

pod2usage({-verbose => 2, -exitval => 0}) if $opts->{man};
pod2usage({-verbose => 1, -exitval => 0}) if $opts->{help};

my $cfg = new Configurator();

# load environment variables
$cfg->load_translations('env') if $opts->{'env'};

# load user specified translations
if (defined $opts->{translation}) {
	$cfg->add_translations(%{$opts->{translation}});
}

# load user specified translation files
if (defined $opts->{load}) {
	foreach (@{$opts->{load}}) {
		$cfg->load_translations($_);
	}
}

my $action = shift;
my $actions = {
	'test' => \&_test,
	'list' => \&_list,
	'check' => \&_check,
	'translate' => \&_translate,
	'parse' => \&_translate,
};

pod2usage() unless defined $action and defined $actions->{$action};

&{$actions->{$action}}(@ARGV);

#===============================================================================
sub _list {
#===============================================================================
	my $tr = $cfg->list_translations();

	say YELLOW, 'Translations loaded:', RESET unless $opts->{'quiet'};
	foreach my $k (sort keys %$tr) {
		my $v = $tr->{$k};

		if ($opts->{'quiet'}) {
			say $k, ' => ', $v;
		} else {
			printf "  [%-30s] => [%s]\n", $k, $v;
		}
	}
}

#===============================================================================
sub _test {
#===============================================================================
	my $tstr = shift;

	my ($report, $res) = $cfg->translate($tstr);

	handle_parser_report($report);

	say GREEN, 'Output', RESET unless $opts->{'quiet'};
	say $res;
}

#===============================================================================
sub _check {
#===============================================================================
	my $folder = dirname(abs_path($0));

	my $if = shift;

	die 'missing input file name' unless defined $if;

	# validate input/output files
	validate_input_file($if);

	# translate input file into output file
	handle_parser_report($cfg->parse($if));
}

#===============================================================================
sub _translate {
#===============================================================================
	my $folder = dirname(abs_path($0));

	my $if = shift;
	my $of = shift;

	die 'missing input file name' unless defined $if;
	die 'missing output file name' unless defined $of;

	# validate input/output files
	validate_input_file($if);
	validate_output_file($of);

	# translate input file into output file and print the report
	handle_parser_report($cfg->parse($if, $of));
}

#===============================================================================
sub read_from_file {
#===============================================================================
	my $file = shift;
	open(my $fh, '<', $file);
	my @lines = <$fh>;
	close($fh);

	return @lines if wantarray;

	return join('', @lines);
}

#===============================================================================
sub send_to_file {
#===============================================================================
	my $file = shift;

	open(my $fh, '>', $file) or die 'failed to write to file [', $file, ']';
	foreach (@_) {
		print $fh $_;
	}
	close($fh);
}

#===============================================================================
sub validate_input_file {
#===============================================================================
	my $if = shift;

	die "no such file [$if]" unless -f $if;
}

#===============================================================================
sub validate_output_file {
#===============================================================================
	my $of = shift;

	my $ofdn = dirname(abs_path($of));

	make_path $ofdn or die "could not create folder [$ofdn]" unless -d $ofdn;
}

#===============================================================================
sub handle_parser_report {
#===============================================================================
	my $tr = shift;

	# pretty print successful translations
	unless ($opts->{'quiet'}) {
		say GREEN, 'Translations used', RESET if keys %{$tr->{'success'}} > 0;
		foreach my $k (sort keys %{$tr->{'success'}}) {
			say GREEN, '  [', RESET, $k, GREEN, '] to [', RESET, $tr->{'success'}->{$k}, GREEN, ']', RESET;
		}
	}

	if (keys %{$tr->{'failed'}} > 0) {
		unless ($opts->{'quiet'}) {
			# pretty print failed translations
			say RED, 'Translations failed', RESET ;
			foreach my $k (sort keys %{$tr->{'failed'}}) {
				say RED, '  [', RESET, $k, RED, '] ', $tr->{'failed'}->{$k}, ' occurrences ', RESET;
			}
		}

		# execution failure
		exit 1 unless $opts->{'ignore-failed'};
	}
}

__END__

=head1 NAME

vs-configurator - Process templates of configuration files and produce final versions

=head1 DESCRIPTION

This script will read translations in the form of key-values and attempt to replace placeholders containing references to said keys with the respective values. This is useful for creating template configuration files with sensitive information such as passwords or other values which depend on the environment the program is suppose to run.

A B<translation> is our chosen term for indicating a key-value tupple. A B<placeholder> is a special sequence of characters which can reference one or more keys (as well as an optional default value if no translation for a key exists), the first key for which there is a translation will replace the whole placeholder with the key's value. If no translation is found then an error will be issued and the output file will not be written/modified.

=head2 Features

=over 4

=item Support for multiple configuration keys on a single placeholder in order to provide fallbacks

=item Support for default values

=item Support for environment variables

=back

=head1 SYNOPSIS

Check if there are translations for all placeholders of a template

  vs-configurator [OPTIONS] check <input file>

Process a template file and write the output to a file

  vs-configurator [OPTIONS] translate <input file> <output file>

List all translations

  vs-configurator [OPTIONS] list

Test the configurator against a sample text. Can also be useful to print information from the translation files.

  vs-configurator [OPTIONS] test '<sample text>'

=head1 OPTIONS

=head2 --ignore-failed

Passing this flag will ignore any missed translation when parsing a template file. This means that the program will terminate with success (exitcode 0) even if some translations were not found. Otherwise, any missing translation will result in the program terminating in error (exitcode 1).

Note that this simply controls the exitcode for when there is a missing translation, the output file will alwas be written. If you wish to run a check before re-creating the output file then run the B<check> action first. The output file will keep any placeholder for which there was no translation found. This means there is no translation into an empty string. Bare this in mind since this can lead to potential configuration failures. This flag is mostly aimed at meta-templating.

=head2 -l, --load <file>

Load a translations file. See section TRANSLATION FILES for more info.

=head2 --no-env

Do not load environment variables as translations.

=head2 --quiet

Hides most STDOUT output. Note that actions which imply some for of output will restrict it to the bare minimum. For example, the I<test> action will only output the translated string, which lets you use it on a script to fetch a translation value.

=head2 -t, --translation <key>=<value>

This allows you set a configuration key and its value. For example, C<--translation my.server.hostname=localhost> would allow replacing all placeholders containing the reference C<config.my.server.hostname> with the value C<localhost>. Note that when specifying the translation you B<must not> include the C<config.> prefix.

=head1 TEMPLATING

The templating system is the most basic placeholder templating system possible. In fact, the goal is that a single regular expression is capable of extracting the placeholder and once a suitable translation is found replace the placeholder with the final value.

Placeholder format: C<{%config.key1:config.key2:env.ENV_VAR:-default value%}>

Configuration key references inside a placeholder must be preceeded by C<config.>, e.g.: C<{%config.my-app.server.hostname%}>

Configuration keys must match the following regular expression: C</\w+([-\.]\w+)*/>

References to environment variables must be preceeded by C<env.>, e.g.: C<{%env.HOME%}>.

Default values can contain any contain any printable character sequence with the exception of newline and the placeholder terminator character sequence C<%}>.

You can escape the opening of a placeholder by using C<\{%>.

=head2 Example

Lets take the input file F<input.conf.dist> with the following contents.

	hostname={%config.host1:config.host2:-localhost%}
	user_folder={%config.folder:env.HOME%}

If you run

  vs-configurator -t host2=127.0.0.1 input.conf.dist input.conf

The output file F<input.conf> will contain

	hostname={%config.host1:config.host2:-localhost%}
	user_folder={%config.folder:env.HOME%}

Note that when you specify a translation you B<must not> include the C<config.> prefix. This is automatic for user specified translations. This is so that you don't accidentally override an environmental variable translations, which are automatically loaded. Also, future automatic translations might require other namespaces.

=head1 TRANSLATION FILES

Instead of providing the configuration key-values as command line arguments you can add them to one or more files and feed these configuration files to the configurator program. For example, create the file F<configurator.cfg> (the file extension must be .cfg) and add the following to its contents:

	; this is a comment
	# this is also a comment
	# note that there is no space between the '=' and the value,
	# if you add a space the value will contain that space
	my-app.name=Some application
	my-app.sql.username=myapp
	my-app.sql.password=myapppwd
	; note that we are now using ':' instead of '=' to separate the key from the value
	; both symbols are supported
	sql.hostname:localhost
	sql.username:dbadmin
	sql.password:dbadminpwd

You can now load this configuration file into the configurator and test a placeholder string

  vs-configurator --load configurator.cfg test '{%config.my-app.sql.hostname:config.sql.hostname%}'

or you can list all the available configuration keys and values

  vs-configurator --load configurator.cfg list

=head1 AUTHOR

JB Ribeiro (Vredens) - E<lt>vredens@gmail.comE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright 2016 JB Ribeiro.

This program is free software; you can redistribute it and/or modify it under the same terms as Perl itself.

The full text of the license can be found in L<http://dev.perl.org/licenses/>.

=cut
