#!/bin/bash

# Again set the right permissions (needed when mounting from a volume)
#chown -Rf www-data.www-data /usr/share/nginx/html/

set > /data/run/env.txt

function create_rw_sub_folder {
	folder=/data/run/$1
	if [ ! -d "$folder" ]; then
		mkdir -p "$folder"
	fi
	chown -Rf 1000:1000 "$folder"
}

# truncate the boot log
> /data/run/boot.log

# allow runtime execution of scripts
#if [ -f /data/run/docker-boot.sh ]; then
	#source /data/run/docker-boot.sh >> /data/run/boot.log 2>&1
#fi

# allow runtime execution of scripts
# ACHTUNG: this is dangerous if executed in production systems, currently WIP to restrict actions possible from a boot script
if [ -f /data/etc/docker-boot.sh ]; then
	source /data/etc/docker-boot.sh >> /data/run/boot.log 2>&1
fi

cd /
exec supervisord -c /opt/docker/etc/supervisor.conf --logfile /dev/null --pidfile /dev/null --user root
