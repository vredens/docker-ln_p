
case "$1" in
	'clean')
		docker rmi $(docker images | grep "^<none>" | awk '{print $3}')
		docker images
	;;

	'build')
		docker build -t vredens/ln_p .
	;;
esac
